import { Users } from '../../submodules/shared-library/models/Users'
import { UserScope } from '../../submodules/shared-library/interfaces/IUser';

const createUserIfNotExist = async (username: string, scope: UserScope) => {
    const user = await Users.findOne({username, scope});
    if(!user) {
        return await Users.create({username, scope})
    } else {
        return user;
    }
}

export { createUserIfNotExist };