import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import sc from '../../../submodules/shared-library/services/steemconnect/steemconnect.service';
import vault from '../../../services/vault/vault.service';
import jwt from '../../../services/jwt/jwt.service';
import { query } from 'express-validator/check';
import { setUserRegistered } from '../../../submodules/shared-library/services/cache/cache';
import { UserScope } from '../../../submodules/shared-library/interfaces/IUser';
import { createUserIfNotExist } from '../../../dataServices/users/createUserIfNotExist';

const middleware: any[] =  [
    query('code').isString()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        console.log('dashboard/authorize', new Date());

        const { code } = req.query;
       
        const { data: { access_token, refresh_token, username } } = await sc.getRefreshToken(code, sc.dashboard.scope);
    
        await vault.storeRefreshToken(username, refresh_token);
        await vault.storeAccessToken(username, access_token, true);

        await setUserRegistered(username);
        const user = await createUserIfNotExist(username, UserScope.DASHBBOARD);
        
        const token = jwt.createJwt(username, UserScope.DASHBBOARD, user.adopter, user.imported);

        const tokenString = encodeURIComponent(token);
        return res.redirect(process.env.DASHBOARD_ADDR + '/authorize?jwt=' + tokenString);
        
    }, req, res);
}

export default {
    middleware,
    handler
}