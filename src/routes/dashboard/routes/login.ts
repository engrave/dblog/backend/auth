import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import sc from '../../../submodules/shared-library/services/steemconnect/steemconnect.service';

const middleware: any[] =  [
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

    	console.log('dashboard/login', new Date());

        const url = sc.dashboard.getLoginURL() + '&response_type=code';

        return res.redirect( url );
        
    }, req, res);
}

export default {
    middleware,
    handler
}