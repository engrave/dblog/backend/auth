declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__: string;
        }
    }
}

global.__rootdir__ = '/app/dist';

import app from './app/app';
import { listenOnPort } from './submodules/shared-library/utils/listenOnPort';
import * as mongoose from 'mongoose';
import { mongo, logger } from './submodules/shared-library';

const Sentry = require('@sentry/node');
import { RewriteFrames } from '@sentry/integrations';

Sentry.init({
    dsn: 'https://a84d20ad98c447d194ec02d5a42a0938@sentry.engrave.dev/7',
    release: `${process.env.npm_package_name}@${process.env.npm_package_version}`,
    integrations: [
        new RewriteFrames({ root: global.__rootdir__ })
    ]
});

( async () => {

    try {
        
        await mongoose.connect(mongo.uri, mongo.options);
    
        listenOnPort(app, 3000);

    } catch (error) {
        logger.warn('Error encountered while starting the application: ', error.message);
        process.exit(1);
    }
})();
