import createJwt from './actions/createJwt';
import validateJwt from './actions/validateJwt';

const jwt = {
    createJwt,
    validateJwt
}

export default jwt;