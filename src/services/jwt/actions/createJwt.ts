import { UserScope } from "../../../submodules/shared-library/interfaces/IUser";

const secrets = require('@cloudreach/docker-secrets');
var jwt = require('jsonwebtoken');

export default (username: string, scope: UserScope, adopter: boolean, imported: boolean) => {
    return jwt.sign({
        data: {
            username: username,
            adopter: adopter,
            imported: imported,
            platform: 'engrave.website',
            scope: scope
        }
      }, secrets.JWT_SECRET, { expiresIn: '7d' });
}